# yasws

**Yet Another Static Web Server** is a simple go webserver written in [Go](https://golang.org/) which takes as input a [markdown file](https://gitlab.com/mellotanica/yasws/wikis/structure) describing the structure of the site you want to publish and creates the structure and index pages to navigate your directory

## Usage

For a single site you can just run the server explicitly passing an optional css file to apply the stylesheet:
```
$> yasws -p 8080 -s mystyle.css 'my/site/' mysite.md
```

this will serve the contents of the local file [`mysite.md`](https://gitlab.com/mellotanica/yasws/wikis/structure) at the path `http://serveraddr/my/site` on port 8080 applying the mystyle.css stylesheet.

All the references are resolved from the current directory.

If you want to serve multiple subpaths simultaneously you must use the [configuration file](https://gitlab.com/mellotanica/yasws/wikis/configuration-file-file) and run the server as
```
$> yasws -c serverconfig.json
```