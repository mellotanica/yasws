package main

import (
	"fmt"
	"gitlab.com/golang-commonmark/markdown"
	"html"
	"io/ioutil"
	"net/url"
	"os"
	"strings"
	"unicode"
)

func validChar(s string, i int) bool {
	if i > 0 && s[i-1] != '\\' {
		return true
	}
	return false
}

func findChar(s string, c string, fd func(string, string) int) int {
	i := 0
	valid := false
	str := s
	inc := 0
	for {
		i = fd(str, c)
		valid = validChar(str, i)
		if valid {
			break
		}
		if i < 0 || i >= len(str) {
			return -1
		}
		i += 1
		str = str[i:]
		inc += i
	}
	return i + inc
}

func findMatching(s string, o string, c string) int {
	if len(s) <= 0 || strings.Index(s, o) != 0 {
		return -1
	}

	ts := s[1:]
	stack := 1
	pos := 1

	for stack > 0 && pos < len(s) {
		no := findChar(ts, o, strings.Index)
		nc := findChar(ts, c, strings.Index)

		if nc < 0 || nc == no {
			return -1
		}

		if no < 0 || no > nc {
			stack--
			ts = ts[nc:]
			pos += nc
		} else if no < nc {
			stack++
			ts = ts[no:]
			pos += no
		}
	}

	return pos
}

func parseBlock(s *site, block string, path string, title string, parent string, parentTitle string) {
	// search for sub blocks and recursively parse them
	for {
		// search for block
		ibstart := findChar(block, "{", strings.Index)
		if ibstart > 0 && block[ibstart-1] == ']' {

			ibend := findMatching(block[ibstart:], "{", "}")
			if ibend < 0 {
				fmt.Fprintf(os.Stderr, "ERROR reading %s: unmatched '{', block: \n%s\n", s.Config, block)
				os.Exit(1)
			}
			ibend += ibstart

			// remove leading whitespaces from block
			ib := block[ibstart+1 : ibend]
			fd := 0
			ws := ""
			for unicode.IsSpace(rune(ib[fd])) {
				fd++
			}
			ws = ib[:fd]
			if ws[0] == '\n' {
				ws = ws[1:]
			}
			if len(ws) > 0 {
				rpl := strings.NewReplacer("\n"+ws, "\n")
				ib = rpl.Replace(ib)
			}

			pb := strings.TrimSpace(block[:ibstart])
			if pb[len(pb)-1] != ']' || pb[len(pb)-2] == '\\' {
				begin := ibstart - 50
				if begin < 0 {
					begin = 0
				}
				end := ibend + 50
				if end < 0 {
					end = len(block)
				}
				fmt.Fprintf(os.Stderr, "ERROR reading %s: missing block name:\n... %s ...\n", s.Config, block[begin:end])
				os.Exit(1)
			}

			namestart := findChar(pb, "[", strings.LastIndex)
			if namestart < 0 {
				fmt.Fprintf(os.Stderr, "ERROR reading %s: unmatched ']'\b", s.Config)
				os.Exit(1)
			}

			subpath := path
			if len(path) > 1 && path[len(path)-1] != '/' {
				subpath += "/"
			}
			subpath += url.QueryEscape(pb[namestart+1 : len(pb)-1])

			parseBlock(s, ib, subpath, pb[namestart+1:len(pb)-1], path, title)

			block = fmt.Sprintf("%s(%s)%s", block[:ibstart], subpath, block[ibend+1:])
		} else {
			break
		}
	}
	// render current block
	md := markdown.New(markdown.XHTMLOutput(true))
	style := ""
	if s.Stylesheet != "" {
		style = fmt.Sprintf("\t\t<style type = \"text/css\">\n%s\n</style>\n", s.Stylesheet)
	}
	back := ""
	if parent != "" {
		back = fmt.Sprintf("\t\t<a class=\"back\" href=\"%s\">%s</a>\n", parent, parentTitle)
	}
	content := fmt.Sprintf(`<html>
    <head>
        <title>%s</title>
%s
    </head>
    <body>
        %s
        <a class="home" href="%s">Home</a>
        <h1 class="title">%s</h1>
		<div class="content">
        %s
		</div>
    </body>
</html>`, html.EscapeString(title), style, back, s.Path, title, md.RenderToString([]byte(block)))
	s.Directories = append(s.Directories, directory{path, title, content, s})
}

func parse(sitepath string, basepath string, stylePath string, siteFile string, title string) *site {
	if _, err := os.Stat(siteFile); os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "ERROR: %s is not a valid site.md file\n", siteFile)
		os.Exit(1)
	}
	mainBlock, err := ioutil.ReadFile(siteFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR reading %s: %s\n", siteFile, err.Error())
		os.Exit(1)
	}

	if basepath[len(basepath)-1] != '/' {
		basepath += "/"
	}

	if sitepath[len(sitepath)-1] != '/' {
		sitepath += "/"
	}

	stylesheet := ""
	if stylePath != "" {
		stylecontent, err := ioutil.ReadFile(stylePath)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERROR reading %s: %s", stylePath, err.Error())
			os.Exit(1)
		}
		stylesheet = string(stylecontent)
	}

	s := site{sitepath, stylesheet, basepath, siteFile, make([]directory, 0)}

	parseBlock(&s, string(mainBlock), sitepath, title, "", "")

	return &s
}
