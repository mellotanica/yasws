package main

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
)

type sitecfg struct {
	Path     *string
	File     *string
	Basepath *string
	Style    *string
	Title    *string
}

type configs []sitecfg

func readConfig(filePath string, basepath string) (sites []*site, basedir string, err error) {
	plain, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	var cfg configs

	err = json.Unmarshal(plain, &cfg)
	if err != nil {
		return
	}

	sites = make([]*site, 0)

	for s, conf := range cfg {
		bp := basepath
		if conf.Basepath != nil {
			bp = *conf.Basepath
		}

		if conf.Path == nil {
			return nil, "", errors.New(fmt.Sprintf("missing 'path' field for site %d", s))
		}

		if conf.File == nil {
			return nil, "", errors.New(fmt.Sprintf("missing 'file' field for site %d", s))
		}

		ss := ""
		if conf.Style != nil {
			ss = *conf.Style
		}

		ttl := ""
		if conf.Title != nil {
			ttl = *conf.Title
		}

		sites = append(sites, parse(*conf.Path, bp, ss, *conf.File, ttl))
	}

	return
}
