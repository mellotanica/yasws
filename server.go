package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

func serve(sites []*site, port int, cert string, key string, bindaddr string) {
	for _, site := range sites {
		log.Printf("site: %s, style: %s, file: %s, basepath: %s dirs:\n", site.Path, site.Stylesheet, site.Config, site.Basepath)
		for id := len(site.Directories) - 1; id >= 0; id-- {
			d := site.Directories[id]
			log.Printf(" - %s\n", d.Path)
			http.HandleFunc(d.Path, func(w http.ResponseWriter, r *http.Request) {
				if r.URL.Path == d.Path {
					fmt.Fprint(w, d.Content)
				} else {
					res := strings.Replace(r.URL.Path, d.Root.Path, d.Root.Basepath, 1)
					http.ServeFile(w, r, res)
				}
			})
		}
	}

	srvAddr := fmt.Sprintf("%s:%d", bindaddr, port)
	log.Printf("serving requests on %s\n", srvAddr)
	if cert != "" && key != "" {
		log.Fatal(http.ListenAndServeTLS(srvAddr, cert, key, nil))
	} else {
		log.Fatal(http.ListenAndServe(srvAddr, nil))
	}
}
