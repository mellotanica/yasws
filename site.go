package main

type directory struct {
	Path    string
	Title   string
	Content string
	Root    *site
}

type site struct {
	Path        string
	Stylesheet  string
	Basepath    string
	Config      string
	Directories []directory
}
