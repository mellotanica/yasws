package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	port := flag.Int("p", 8080, "run the webserver on this port")
	addr := flag.String("a", "127.0.0.1", "bind the webserver to this address")
	style := flag.String("s", "", "default stylesheet to apply")
	title := flag.String("t", "Root", "main page title")
	config := flag.String("c", "", "custom configuration (useful for multiple subpaths)")
	basedir := flag.String("b", ".", "base directory containing files")
	srvCert := flag.String("cert", "", "SSL server certificate file")
	srvKey := flag.String("key", "", "SSL server key file")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [OPTIONS] <site.md>\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "\n<site.md> is ignored if custom config is enabled\n\nOPTIONS:\n")
		flag.PrintDefaults()
	}

	flag.Parse()

	var sites []*site

	if _, err := os.Stat(*config); !os.IsNotExist(err) {
		sites, *basedir, err = readConfig(*config, *basedir)
		if err != nil {
			flag.Usage()
			fmt.Fprintf(os.Stderr, "\nERROR: reading config file: %s\n", err.Error())
			os.Exit(1)
		}
	} else {
		if _, err := os.Stat(*basedir); os.IsNotExist(err) {
			flag.Usage()
			fmt.Fprintf(os.Stderr, "\nERROR: basedir is invalid: %s\n", err.Error())
			os.Exit(1)
		}
		sites = make([]*site, 0)
		if flag.NArg() > 0 {
			if _, err := os.Stat(flag.Arg(0)); !os.IsNotExist(err) {
				sites = append(sites, parse("/", *basedir, *style, flag.Arg(0), *title))
			}
		}
	}

	if len(sites) <= 0 {
		flag.Usage()
		fmt.Fprintf(os.Stderr, "\nERROR: no valid site configured\n")
		os.Exit(1)
	}

	if *srvCert != "" || *srvKey != "" {
		_, certErr := os.Stat(*srvCert)
		_, keyErr := os.Stat(*srvKey)
		if os.IsNotExist(certErr) || os.IsNotExist(keyErr) {
			flag.Usage()
			fmt.Fprintf(os.Stderr, "\nERROR: both cert and key file must be valid for SSL to work\n")
			os.Exit(1)
		}
	}

	serve(sites, *port, *srvCert, *srvKey, *addr)
}
